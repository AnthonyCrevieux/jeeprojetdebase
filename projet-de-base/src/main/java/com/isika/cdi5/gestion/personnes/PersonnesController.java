package com.isika.cdi5.gestion.personnes;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.isika.cdi5.gestion.personnes.model.Adresse;
import com.isika.cdi5.gestion.personnes.model.Personne;

@Named
@RequestScoped
public class PersonnesController {

	@Inject
	private PersonnesService personnesService;
	
	private Personne personne = new Personne();
	private Adresse adresse = new Adresse();
	private List<Personne> listePersonnes;
	
	@PostConstruct
	private void chargerListePersonnes() {
		listePersonnes = personnesService.rechercheToutesPersonnes();
	}
	
	public void eventAjoutPersonneDetecte(@Observes(notifyObserver = Reception.IF_EXISTS) final Personne personne) {
		chargerListePersonnes();
	}
	
	public void createPersonne() {
		
		// On affecte l'adresse (jusque là indépendante à la personne qu'on va créer)
		// Hibernate va l'enregistrer puisque nous avons configuré la cascade sur l'entité personne
		personne.associerAdresse(adresse);
		
		// On persiste
		Long id = personnesService.creerPersonne(personne);
		
		// Message à afficher sur la page
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Personne enregistrée", "Id [" + id + "]");
        FacesContext.getCurrentInstance().addMessage(null, m);
	}
	
	public void supprimerPersonne(Personne selectedPersonne) {
		boolean suppression = personnesService.supprimerPersonne(selectedPersonne.getId());
		if(suppression) {
			showMessageAndReloadData("Personne supprimée");
		}
	}
	
	public void modifierPersonne(Personne selected) {
		Personne modifiee = personnesService.modifier(selected);
		if(modifiee != null) {
			showMessageAndReloadData("Personne modifiée");
		}
	}

	private void showMessageAndReloadData(String message) {
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, message, "");
		FacesContext.getCurrentInstance().addMessage(null, m);
		chargerListePersonnes();
	}
	
	public Personne getPersonne() {
		return personne;
	}
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	
	public Adresse getAdresse() {
		return adresse;
	}
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	
	public List<Personne> getListePersonnes() {
		return listePersonnes;
	}
}
