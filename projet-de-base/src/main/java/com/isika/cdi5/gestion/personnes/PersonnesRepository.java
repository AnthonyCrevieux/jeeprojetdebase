package com.isika.cdi5.gestion.personnes;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.isika.cdi5.gestion.personnes.model.Personne;

@Stateless
public class PersonnesRepository {

	private static final Logger LOGGER = Logger.getLogger(PersonnesRepository.class.getSimpleName());

	@PersistenceContext
	private EntityManager entityManager;

	@Inject
	private Event<Personne> personnesEventSource;

	public Long creer(Personne personne) {
		this.entityManager.persist(personne);
		this.entityManager.flush();

		// On notifie les composants qui écoutent des evts sur l'ajout de Personne
		// qu'une entité vient d'être persistée
		personnesEventSource.fire(personne);
		
		// Message informatif pour les logs serveur
		LOGGER.info("Persisted personne : " + personne);
		return personne.getId();
	}

	public List<Personne> findAll() {
		return this.entityManager.createNamedQuery("Personne.findAll", Personne.class).getResultList();
	}

	public boolean remove(Personne personne) {
		this.entityManager.remove(personne);
		return true;
	}

	public Personne findById(Long id) {
		return this.entityManager.find(Personne.class, id);
	}

	public Personne update(Personne selected) {
		return this.entityManager.merge(selected);
	}

}
