package com.isika.cdi5.gestion.personnes;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.isika.cdi5.gestion.personnes.model.Personne;

@Stateless
public class PersonnesService {

	@Inject
	private PersonnesRepository personnesRepository;
	
	public Long creerPersonne(Personne personne) {
		
		// TODO : vu que c'est un service métier on peut faire plus de choses 
		// que juste persister
		
		return personnesRepository.creer(personne);
	}
	
	public List<Personne> rechercheToutesPersonnes() {
		return personnesRepository.findAll();
	}

	public boolean supprimerPersonne(Long id) {
		Personne personneById = personnesRepository.findById(id);
		if(personneById != null) {
			return personnesRepository.remove(personneById);
		}
		return false;
	}

	public Personne modifier(Personne selected) {
		return personnesRepository.update(selected);
	}
	
}
